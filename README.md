# Trajektorien des menschlichen Aufstehens für die Entwicklung eines kooperativen Assistenzsystems

## What is in the workspace

This workspace contains five simulations of two robots, the "simple robot" and the KUKA robot, following predefined trajectories. The packages are named according to the degrees of freedom (DoF) required by the "simple robot" to perform each motion: one simulation for 1 DoF, two for 2 DoF, and two for 3 DoF. Each movement can be executed with one of two velocity profiles—constant or trapezoidal.

## Setup for the Kuka robots

Quick guide for setting up the Kuka FRI with Ubuntu and ros2.

Instructions for installing and using the repo are described below in the README. 

Additionally for inverse kinematics

1. Follow Installation instructions: 
```
https://github.com/NxRLab/ModernRobotics/tree/master/packages/Python
```
2. Run
```
pip install networkx==2.4 urdfpy mr-urdf-loader
```

## Getting started

***Required setup : Ubuntu 22.04 LTS***

1.  Install `ros2` packages. The current development is based of `ros2 humble`. Installation steps are described [here](https://docs.ros.org/en/humble/Installation.html).
2. Source your `ros2` environment:
    ```shell
    source /opt/ros/humble/setup.bash
    ```
    **NOTE**: The ros2 environment needs to be sources in every used terminal. If only one distribution of ros2 is used, it can be added to the `~/.bashrc` file.
3. Install `colcon` and its extensions :
    ```shell
    sudo apt install python3-colcon-common-extensions
     ```
4. Pull relevant packages, install dependencies, compile, and source the workspace by using:
    ```shell
    git clone https://gitlab.kit.edu/uquzq/trajektorien-des-menschlichen-aufstehens-fuer-die-entwicklung-eines-kooperativen-assistenzsystems.git
    cd ~/trajektorien-des-menschlichen-aufstehens-fuer-die-entwicklung-eines-kooperativen-assistenzsystems/ros2_ws
    rosdep install --ignore-src --from-paths . -y -r
    colcon build --cmake-args -DCMAKE_BUILD_TYPE=Release --symlink-install
    source install/setup.bash
    ```

## Launching a simulation

1. In the first tab navigate to the launch folder of the KUKA iiwa robot:
    ```shell
    cd ~/trajektorien-des-menschlichen-aufstehens-fuer-die-entwicklung-eines-kooperativen-assistenzsystems/ros2_ws/src/iiwa/iiwa_bringup/launch
    ```
2. Launch the RViz simulation with the KUKA iiwa robot: 
    ```shell
    ros2 launch iiwa.launch.py
    ```
3. In a separate tab navigate to the workspace and source the environment:
    ```shell
    cd ~/trajektorien-des-menschlichen-aufstehens-fuer-die-entwicklung-eines-kooperativen-assistenzsystems/ros2_ws/
    source install/setup.bash
    ```
4. And launch one of the simulations with the simple robot (ex: 1 degree of freedom):
    ```shell
    ros2 launch simple_robot_1dof simple_robot_rviz_simulation_1dof_launch.py
    ```
This will open a separate RViz window with a "simple robot" that performs a repeating motion. In this simulation, the KUKA robot will execute the motion only once, timed to align with the second cycle of the simple robot’s movement.

## Parameters 

There are parameters that can be defined during the launching, that define the velocity profile and the exact movement: 

- `constant_speed` - Specifies which speed profile should be executed. Set to `true` for a constant velocity profile, or `false` for a trapezoidal profile.
- `simultaneous`  - Determines whether the two movements are performed simultaneously (applicable only for 2 and 3 DoF movements). Set to `true` for simultaneous movement, or `false` for sequential movement.
- `velocity_constant`  - Sets the velocity for the constant velocity profile (float).
- `acceleration, decelaration, max_velocity` - Define the acceleration, deceleration, and maximum velocity values for the trapezoidal velocity profile (float).
