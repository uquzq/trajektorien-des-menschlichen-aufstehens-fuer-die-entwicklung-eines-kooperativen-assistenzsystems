import os

import rclpy
from rclpy.node import Node
from geometry_msgs.msg import Pose
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from modern_robotics import IKinBody, RpToTrans, FKinBody
import numpy as np
from ament_index_python.packages import get_package_share_directory
from mr_urdf_loader import loadURDF
from scipy.spatial.transform import Rotation

class InverseKinematicsNode(Node):
    def __init__(self):
        super().__init__('inverse_kinematics_node')
        self.subscription = self.create_subscription(
            Pose,
            '/kuka_iiwa/target_pose',
            self.pose_callback,
            10)
        self.publisher = self.create_publisher(JointTrajectory, '/iiwa_arm_controller/joint_trajectory', 10)

        # load KUKA URDF
        urdf_file_path = os.path.join(
            get_package_share_directory('iiwa_description'), 'urdf', 'iiwa14.urdf')
        self.get_logger().info(f'Loading URDF from: {urdf_file_path}')
        self.robot_model = loadURDF(urdf_file_path)

        self.M = self.robot_model["M"]
        self.Slist = self.robot_model["Slist"]
        self.Mlist = self.robot_model["Mlist"]
        self.Glist = self.robot_model["Glist"]
        self.Blist = self.robot_model["Blist"]

        self.joint_names = [f'joint_a{i+1}' for i in range(len(self.Slist[0]))]

        self.joint_angles = np.zeros(len(self.Slist[0]))
        # forward kinematics
        T = FKinBody(self.M, self.Blist, self.joint_angles)
        rotation_matrix = T[:3, :3]
        quat = Rotation.from_matrix(rotation_matrix).as_quat()
        position = T[:3, 3]
        self.get_logger().info('T: "%s":' %T)

        self.get_logger().info(f'Calculated Forward Kinematics Quaternion: {quat}')
        self.get_logger().info(f'Calculated Forward Kinematics Position: {position}')

    def pose_callback(self, pose: Pose):
        quat = [pose.orientation.x, pose.orientation.y, pose.orientation.z, pose.orientation.w]
        r = Rotation.from_quat(quat)
        rotation_matrix = r.as_matrix()
        T_sd = RpToTrans(rotation_matrix, np.array([pose.position.x, pose.position.y, pose.position.z]))
        eomg = 1e-4  # error limit for the angle
        ev = 1e-4  # error limit for the translation

        # define list of angles that are close to satisfying the pose
        self.joint_angles[1] = np.pi/2
        self.joint_angles[2] = 0.0
        self.joint_angles[3] = np.pi/2
        self.joint_angles[4] = 0.0
        self.joint_angles[5] = np.pi/2
        self.joint_angles[6] = 0.0
       
       # call the IKin function from modern robotics library
        joint_angles, success = IKinBody(self.Blist, self.M, T_sd, self.joint_angles, eomg, ev)
       

        if success:
            self.joint_angles = joint_angles

            joint_trajectory_msg = JointTrajectory()
            joint_trajectory_msg.joint_names = self.joint_names
            point = JointTrajectoryPoint()
            point.positions = joint_angles.tolist()
            # point.time_from_start.sec = 1 # Example time, adjust as needed
            point.time_from_start.nanosec = 450000000
            joint_trajectory_msg.points.append(point)

            self.publisher.publish(joint_trajectory_msg)
        else:
            self.get_logger().error("Inverse Kinematics failed to find a solution.")


def main(args=None):
    rclpy.init(args=args)
    node = InverseKinematicsNode()
    rclpy.spin(node)
    node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
