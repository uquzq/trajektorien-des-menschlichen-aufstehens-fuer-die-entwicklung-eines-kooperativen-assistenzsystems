import os
from launch import LaunchDescription
from launch_ros.actions import Node
from launch.actions import DeclareLaunchArgument
from ament_index_python.packages import get_package_share_directory
from launch.substitutions import LaunchConfiguration


def generate_launch_description():

    package_name = 'simple_robot_1dof'

    rviz_config_file = os.path.join(
        get_package_share_directory(package_name),
        'rviz',
        'simple_robot_simulation.rviz'
    )

    urdf_dir = os.path.join(get_package_share_directory(package_name), 'urdf')
    urdf_file = os.path.join(urdf_dir, 'simple_robot.urdf')
    with open(urdf_file, 'r') as infp:
        robot_desc = infp.read()



    declared_arguments = []
    declared_arguments.append(
        DeclareLaunchArgument(
            'max_velocity',
            default_value = '0.01725',
            )
    )
    declared_arguments.append(
        DeclareLaunchArgument(
            'acceleration',
            default_value = '0.001327',
            )
    )
    declared_arguments.append(
        DeclareLaunchArgument(
            'decelaration',
            default_value = '0.001327',
            )
    )
    declared_arguments.append(
        DeclareLaunchArgument(
            'velocity_constant',
            default_value = '0.014',
            )
    )
    declared_arguments.append(
        DeclareLaunchArgument(
            'constant_speed',
            default_value = 'False',
            )
    )

    max_velocity = LaunchConfiguration('max_velocity')
    acceleration = LaunchConfiguration('acceleration')
    decelaration = LaunchConfiguration('decelaration')
    velocity_constant = LaunchConfiguration('velocity_constant')
    constant_speed = LaunchConfiguration('constant_speed')


    transformation_node = Node(
            package = package_name,
            executable = 'transformation_node_1dof',
            name =  'transformation_node',
            output = 'screen',
            parameters = []
        )
    reference_node = Node(
            package = package_name,
            executable = 'reference_node_1dof',
            name =  'reference_node',
            output = 'screen',
            parameters = [
                {'max_velocity': max_velocity},
                {'acceleration': acceleration},
                {'decelaration': decelaration},
                {'velocity_constant': velocity_constant},
                {'constant_speed': constant_speed}
            ]
        )
    rviz_node = Node(
            package='rviz2',
            executable='rviz2',
            name='rviz2',
            output='screen',
            arguments=['-d', rviz_config_file]
        )
    kuka_inverse_kinematics_node = Node(
            package= 'iiwa_inverse_kinematics',
            executable='inverse_kinematics_node',
            name='inverse_kinematics',
            output='screen',
            parameters = []
        )
    robot_state_publisher = Node(
        package='robot_state_publisher',
        executable='robot_state_publisher',
        output='both',
        parameters=[
            {'robot_description': robot_desc},
            ]
    )
    nodes = [
        transformation_node,
        reference_node,
        rviz_node,
        kuka_inverse_kinematics_node,
        robot_state_publisher,
    ]

    return LaunchDescription(declared_arguments + nodes)