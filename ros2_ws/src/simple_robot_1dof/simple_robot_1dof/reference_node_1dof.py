import rclpy
from rclpy.node import Node
import numpy as np
from std_msgs.msg import Float64


class Reference_Node_1DoF(Node):

    def __init__(self):
        super().__init__('reference_node_1dof')
        # publishers
        self.publisher_ = self.create_publisher(Float64, '/dof1/reference_angles', 10)
        self.delta_publisher_ = self.create_publisher(Float64, '/delta', 10)

        # parameters
        self.declare_parameter('max_velocity', 0.0)
        self.declare_parameter('acceleration', 0.0)
        self.declare_parameter('decelaration', 0.0)
        self.declare_parameter('velocity_constant', 0.0)
        self.declare_parameter('constant_speed', False)

        self.max_velocity = self.get_parameter('max_velocity').get_parameter_value().double_value
        self.acceleration = self.get_parameter('acceleration').get_parameter_value().double_value
        self.decelaration = self.get_parameter('decelaration').get_parameter_value().double_value
        self.velocity_constant = self.get_parameter('velocity_constant').get_parameter_value().double_value
        self.constant_speed = self.get_parameter('constant_speed').get_parameter_value().bool_value

        # min. and max. values of parameter k
        self.k_start = 0.0
        self.k_end = 1.0
        self.k = self.k_start
        # variable for the velocity
        self.current_velocity = 0.0
        # faktor for the decelaration phase
        self.faktor = 1.3
        # timer period and timer
        self.timer_period = 0.1  # seconds
        self.timer = self.create_timer(self.timer_period, self.ref_point_publisher)
        # List the points
        
    def ref_point_publisher(self):
        # Ref position over the time
        # Interpolate between x_0 - x_1
               
        msg = Float64()

        # const. speed
        if self.constant_speed:
            self.k = self.k + self.velocity_constant  
        # trapez. velocity
        else:
            # acceleration phase
            if self.current_velocity < self.max_velocity and self.k < 0.5:
                self.k = self.k + self.current_velocity
                self.current_velocity = self.current_velocity + self.acceleration
                self.transition_point = self.k
            # constant velocity
            elif self.k >= self.k_end - self.faktor * self.transition_point:
                self.k = self.k + self.current_velocity
                self.current_velocity = self.current_velocity - self.decelaration
            # decelaration phase
            else:
                self.current_velocity = self.max_velocity
                self.k = self.k + self.current_velocity

        #setting back to 0
        if self.k > self.k_end:
            self.k = self.k_start
            self.current_velocity = 0.0


        msg.data = self.k
        self.publisher_.publish(msg)
        

def main(args = None):
    rclpy.init(args = args)
    node = Reference_Node_1DoF()
    rclpy.spin(node)
    node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main() 
