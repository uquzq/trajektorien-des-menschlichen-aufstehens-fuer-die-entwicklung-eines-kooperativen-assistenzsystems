import os
from glob import glob

from setuptools import find_packages, setup

package_name = 'simple_robot_2dof'

setup(
    name=package_name,
    version='0.0.0',
    packages=find_packages(exclude=['test']),
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        ('share/' + package_name + '/launch', ['launch/simple_robot_rviz_simulation_2dof_launch.py']),
        ('share/' + package_name + '/rviz', ['rviz/simple_robot_simulation.rviz']),
        (os.path.join('share', package_name, 'urdf'), glob('urdf/*.urdf')),
        (os.path.join('share', package_name, 'launch'), glob(os.path.join('launch', '*launch.[pxy][yma]*'))),


    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='ventsi',
    maintainer_email='ventsi@todo.todo',
    description='TODO: Package description',
    license='Apache-2.0',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'transformation_node_2dof_sim = simple_robot_2dof.transformation_node_2dof_simultaneous:main',
            'transformation_node_2dof_suc = simple_robot_2dof.transformation_node_2dof_successive:main',
            'reference_node_2dof = simple_robot_2dof.reference_node_2dof:main',
        ],
    },
)
