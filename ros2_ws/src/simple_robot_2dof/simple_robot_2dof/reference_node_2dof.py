import rclpy
from rclpy.node import Node
import numpy as np
from std_msgs.msg import Float64
from geometry_msgs.msg import Point, PoseStamped
import time


class Reference_Node_2DoF(Node):

    def __init__(self):
        super().__init__('reference_node_2dof')
        self.publisher_ = self.create_publisher(Float64, '/dof2/reference_angles', 10)

        self.declare_parameter('max_velocity', 0.0)
        self.declare_parameter('acceleration', 0.0)
        self.declare_parameter('decelaration', 0.0)
        self.declare_parameter('velocity_constant', 0.0)
        self.declare_parameter('constant_speed', False)

        self.max_velocity = self.get_parameter('max_velocity').get_parameter_value().double_value
        self.acceleration = self.get_parameter('acceleration').get_parameter_value().double_value
        self.decelaration = self.get_parameter('decelaration').get_parameter_value().double_value
        self.velocity_constant = self.get_parameter('velocity_constant').get_parameter_value().double_value
        self.constant_speed = self.get_parameter('constant_speed').get_parameter_value().bool_value

        self.index = 0

        self.k_start = 0.0
        self.k_end = 1.0
        self.k = self.k_start
        self.transition_point = 0.0
        self.timer_period = 0.1
        self.faktor = 1.285
        self.timer = self.create_timer(self.timer_period, self.ref_point_publisher)

        self.current_velocity = self.timer_period * self.acceleration


    def ref_point_publisher(self):
        # Ref position over the time
        # Interpolate between x_0 - x_1
               
        msg = Float64()

        # const. velocity
        if self.constant_speed == True:
               self.k = self.k + self.velocity_constant  
        # trapez. velocity
        elif self.constant_speed == False:
            if self.current_velocity < self.max_velocity and self.k < 0.5:
               self.k = self.k + self.current_velocity
               self.current_velocity = self.current_velocity + self.acceleration
               self.transition_point = self.k
            elif self.k > 1 - self.faktor * self.transition_point:
               self.k = self.k + self.current_velocity
               self.current_velocity = self.current_velocity - self.decelaration
            else:
               self.current_velocity = self.max_velocity
               self.k = self.k + self.current_velocity

        if self.k>self.k_end:
            self.k = self.k_start
            self.current_velocity = self.timer_period * self.acceleration
            self.index += 1

        msg.data = self.k

        self.publisher_.publish(msg)
        

def main(args = None):
    rclpy.init(args = args)
    node = Reference_Node_2DoF()
    rclpy.spin(node)
    node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main() 