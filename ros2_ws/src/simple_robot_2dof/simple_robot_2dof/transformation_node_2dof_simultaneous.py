import rclpy
import os
from modern_robotics import FKinBody
from ament_index_python.packages import get_package_share_directory
from mr_urdf_loader import loadURDF
from rclpy.node import Node
from math import sin, cos, pi
import numpy as np
from std_msgs.msg import Float64
from nav_msgs.msg import Path
from sensor_msgs.msg import JointState
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Pose
from visualization_msgs.msg import Marker
from scipy.spatial.transform import Rotation
from rclpy.clock import Clock


class Forward_Kinematics_Node_2Dof(Node):

    def __init__(self):
        super().__init__('forward_kinematics_node_2dof')
        # subscriber
        self.subscription = self.create_subscription(
            Float64,
            '/dof2/reference_angles',
            self.listener_callback,
            10)
        
        urdf_file_path = os.path.join(
            get_package_share_directory('iiwa_description'), 'urdf', 'iiwa14.urdf')
        self.robot_model = loadURDF(urdf_file_path)
        self.M = self.robot_model["M"]
        self.Slist = self.robot_model["Slist"]
        self.Mlist = self.robot_model["Mlist"]
        self.Glist = self.robot_model["Glist"]
        self.Blist = self.robot_model["Blist"]

        urdf_file_path_simple_robot = os.path.join(
            get_package_share_directory('simple_robot_2dof'), 'urdf', 'simple_robot.urdf')
        self.simple_robot_model = loadURDF(urdf_file_path_simple_robot)
        self.simple_robot_M = self.simple_robot_model["M"]
        self.simple_robot_Slist = self.simple_robot_model["Slist"]
        self.simple_robot_Mlist = self.simple_robot_model["Mlist"]
        self.simple_robot_Glist = self.simple_robot_model["Glist"]
        self.simple_robot_Blist = self.simple_robot_model["Blist"]
        self.simple_robot_joint_angles = np.zeros(len(self.simple_robot_Slist[0]))


        self.path_publisher_ = self.create_publisher(Path, '/simple_robot/visualization_path', 10)
        self.point_publisher_ = self.create_publisher(Marker, '/simple_robot/visualization_marker', 10)
        self.joint_state_publisher_simple_robot_ = self.create_publisher(JointState, '/joint_states', 10)
        self.pose_publisher_kuka_robot_ = self.create_publisher(Pose, '/kuka_iiwa/target_pose', 10)

        #RViz
        self.path = Path()
        self.path.header.frame_id = "map"
        self.path.header.stamp = self.get_clock().now().to_msg()

        self.index = 0

    def listener_callback(self, msg):
        # Get the message
        self.k = msg.data

        if self.k == 0:
            self.index += 1
        
        # call the transformation function
        self.transformation_simple_robot()
    

    def transformation_simple_robot(self):
        # define the desired rotation 
        self.theta = - pi / 2
        self.theta_fraction = self.k * self.theta

        # definte the desired height
        self.h = 0.3
        self.h_fraction = self.k * self.h

        self.intermediate_rotation = np.array([
            [cos(self.theta_fraction), -sin(self.theta_fraction), 0, 0],
            [sin(self.theta_fraction), cos(self.theta_fraction), 0, 0],
            [0, 0, 1, self.h_fraction],
            [0, 0, 0, 1]
        ])

        start_forward_kinematics_simple = FKinBody(self.simple_robot_M, self.simple_robot_Blist, self.simple_robot_joint_angles)

        end_forward_kinematics_simple = np.matmul(self.intermediate_rotation, start_forward_kinematics_simple)

        end_position_simple = end_forward_kinematics_simple[:4, 3]
        
        self.publish_joint_states_simple_robot()

        self.construct_path(end_position_simple)
        
        if self.index == 1:
            self.transformation_kuka()        
        
    def transformation_kuka(self):

        kuka_joint_angles = np.zeros(len(self.Slist[0]))
        kuka_joint_angles[0] = np.pi
        kuka_joint_angles[1] = np.pi/2
        kuka_joint_angles[3] = np.pi/2
        kuka_joint_angles[5] = np.pi/2

        start_forward_kinematics_kuka = FKinBody(self.M, self.Blist, kuka_joint_angles)

        end_forward_kinematics_kuka = np.matmul(self.intermediate_rotation, start_forward_kinematics_kuka)

        end_position_kuka = end_forward_kinematics_kuka[:4, 3]
        end_orientation_kuka = Rotation.from_matrix(end_forward_kinematics_kuka[:3, :3]).as_quat()

        self.publish_pose_kuka_pose(end_position_kuka, end_orientation_kuka)         

    def construct_path(self, vector):
        # RViz
        pose = PoseStamped()
        pose.header.frame_id = "map"
        pose.header.stamp = self.get_clock().now().to_msg()
        pose.pose.position.x = vector[0]
        pose.pose.position.y = vector[1]
        pose.pose.position.z = vector[2]
        pose.pose.orientation.w = 1.0

        if self.k == 0:
            self.path.poses.clear()
        else:
            self.path.poses.append(pose)

        self.path_publisher_.publish(self.path)

        self.publish_marker(pose.pose)

    def publish_marker(self, current_pose):
        marker = Marker()
        marker.header.frame_id = "map"
        marker.header.stamp = self.get_clock().now().to_msg()
        marker.type = Marker.SPHERE
        marker.action = Marker.ADD
        marker.pose = current_pose
        marker.scale.x = 0.025
        marker.scale.y = 0.025
        marker.scale.z = 0.025
        marker.color.a = 1.0
        marker.color.r = 0.0
        marker.color.g = 0.0
        marker.color.b = 0.75

        self.point_publisher_.publish(marker)

    def publish_joint_states_simple_robot(self):

        joint_names = ['joint1', 'joint2']
        joint_states = [0.0, 0.0]
        joint_states[1] = self.h_fraction
        joint_states[0] = self.theta_fraction
        
        joint_state_msg = JointState()
        joint_state_msg.header.stamp = Clock().now().to_msg()

        joint_state_msg.name = joint_names
        joint_state_msg.position = joint_states

        self.joint_state_publisher_simple_robot_.publish(joint_state_msg)



    def publish_pose_kuka_pose(self, vector, quaternion):    
        pose_msg = Pose()
        pose_msg.position.x = vector[0]
        pose_msg.position.y = vector[1]
        pose_msg.position.z = vector[2]
        pose_msg.orientation.x = quaternion[0]
        pose_msg.orientation.y = quaternion[1]
        pose_msg.orientation.z = quaternion[2]
        pose_msg.orientation.w = quaternion[3]
        self.pose_publisher_kuka_robot_.publish(pose_msg)   


def main(args = None):
    rclpy.init(args = args)
    node = Forward_Kinematics_Node_2Dof()
    rclpy.spin(node)
    node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()