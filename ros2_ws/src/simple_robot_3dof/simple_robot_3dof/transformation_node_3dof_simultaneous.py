import rclpy
import os
from modern_robotics import FKinBody
from ament_index_python.packages import get_package_share_directory
from mr_urdf_loader import loadURDF
from rclpy.node import Node
from math import sin, cos, pi
import numpy as np
from std_msgs.msg import Float64
from nav_msgs.msg import Path
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Pose
from scipy.spatial.transform import Rotation
from rclpy.clock import Clock


class Forward_Kinematics_Node_3Dof(Node):

    def __init__(self):
        super().__init__('forward_kinematics_node_3dof')
        self.subscription = self.create_subscription(
            Float64,
            '/dof3/reference_angles',
            self.listener_callback,
            10)
        
        urdf_file_path = os.path.join(
            get_package_share_directory('iiwa_description'), 'urdf', 'iiwa14.urdf')
        self.robot_model = loadURDF(urdf_file_path)
        self.M = self.robot_model["M"]
        self.Slist = self.robot_model["Slist"]
        self.Mlist = self.robot_model["Mlist"]
        self.Glist = self.robot_model["Glist"]
        self.Blist = self.robot_model["Blist"]
        self.kuka_joint_angles = np.zeros(len(self.Slist[0]))
        self.kuka_joint_angles[0] = np.pi
        self.kuka_joint_angles[1] = np.pi/2
        self.kuka_joint_angles[3] = np.pi/2
        self.kuka_joint_angles[5] = np.pi/2

        urdf_file_path_simple_robot = os.path.join(
            get_package_share_directory('simple_robot_3dof'), 'urdf', 'simple_robot.urdf')
        self.simple_robot_model = loadURDF(urdf_file_path_simple_robot)
        self.simple_robot_M = self.simple_robot_model["M"]
        self.simple_robot_Slist = self.simple_robot_model["Slist"]
        self.simple_robot_Mlist = self.simple_robot_model["Mlist"]
        self.simple_robot_Glist = self.simple_robot_model["Glist"]
        self.simple_robot_Blist = self.simple_robot_model["Blist"]
        self.simple_robot_joint_angles = [0.0, 0.0, 0.0]

        self.joint_state_publisher_simple_robot_ = self.create_publisher(JointState, '/joint_states', 10)
        self.pose_publisher_kuka_robot_ = self.create_publisher(Pose, '/kuka_iiwa/target_pose', 10)

        #RViz
        self.path = Path()
        self.path.header.frame_id = "map"
        self.path.header.stamp = self.get_clock().now().to_msg()

        self.index = 0

    def listener_callback(self, msg):
        self.k = msg.data

        if self.k == 0:
            self.index += 1

        self.transformation_simple_robot()
        
    def transformation_simple_robot(self):
        # define the desired rotation 
        self.theta =  - pi / 2
        # split theta for interpolation
        self.theta_fraction = self.k * self.theta

        # definte the desired height
        self.h = 0.15
        self.h_fraction = self.k * self.h

        # define the desired length
        self.d = 0.12
        self.d_fraction = self.k * self.d

        self.intermediate_translation = np.array([
            [cos(self.theta_fraction), -sin(self.theta_fraction), 0, 0],
            [sin(self.theta_fraction), cos(self.theta_fraction), 0, self.d_fraction],
            [0, 0, 1, self.h_fraction],
            [0, 0, 0, 1]
        ])
        self.publish_joint_states_simple_robot()


        if self.index == 1:
            self.transformation_kuka()


    def transformation_kuka(self):

        start_forward_kinematics_kuka = FKinBody(self.M, self.Blist, self.kuka_joint_angles)

        self.end_forward_kinematics_kuka_1 = np.matmul(self.intermediate_translation, start_forward_kinematics_kuka)

        end_position_kuka_1 = self.end_forward_kinematics_kuka_1[:4, 3]
        end_orientation_kuka_1 = Rotation.from_matrix(self.end_forward_kinematics_kuka_1[:3, :3]).as_quat()
        self.publish_pose_kuka_pose(end_position_kuka_1, end_orientation_kuka_1)

    def publish_joint_states_simple_robot(self):

        joint_names = ['joint1', 'joint2', 'joint3']
        joint_states = [0.0, 0.0, 0.0]
        joint_states[0] = self.d_fraction
        joint_states[1] = self.h_fraction
        joint_states[2] = self.theta_fraction
        
        joint_state_msg = JointState()
        joint_state_msg.header.stamp = Clock().now().to_msg()

        joint_state_msg.name = joint_names
        joint_state_msg.position = joint_states

        self.joint_state_publisher_simple_robot_.publish(joint_state_msg)


    def publish_pose_kuka_pose(self, vector, quaternion):    
        pose_msg = Pose()
        pose_msg.position.x = vector[0]
        pose_msg.position.y = vector[1]
        pose_msg.position.z = vector[2]
        pose_msg.orientation.x = quaternion[0]
        pose_msg.orientation.y = quaternion[1]
        pose_msg.orientation.z = quaternion[2]
        pose_msg.orientation.w = quaternion[3]
        self.pose_publisher_kuka_robot_.publish(pose_msg)

def main(args = None):
    rclpy.init(args = args)
    node = Forward_Kinematics_Node_3Dof()
    rclpy.spin(node)
    node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()